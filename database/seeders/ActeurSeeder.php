<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActeurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('acteurs')->insert([
        'firstname'=>'Titi',
        'lastname'=>'Grosminet',
        'date adhésion'=>24/02/2021,
        'email'=>'simplon31@laposte.fr',
        ]);
}
}