<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class SujetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tableau = array("Cyrano", "Antigone", "Hamlet", "Roméo et Juliette", "Dom Juan");
        foreach($tableau as $sujet) {
            DB::table('sujets')->insert([
                'description'=>$sujet,
                'date ajout'=>'24/02/2021',
                'status'=> 0,
                ]);
        }
        
    }
    }
    

