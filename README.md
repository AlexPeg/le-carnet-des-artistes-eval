### Version en ligne
* Aller voir la Version en Ligne
* https://damp-ocean-44348.herokuapp.com/




### Installation en local


* Cloner le projet puis faire composer install


* Créer une base de données dans PhpMyadmin (utf8mb4_unicode_ci)


* Modifier le .env.example en .env et en modifiant : APP_NAME, DB_DATABASE, DB_USERNAME, DB_PASSWORD


* php artisan key:generate


(ça devrait remplir le APP_Key du fichier .env)


* lancer les migrations et seeders


* php artisan serve



### Déploiement sur heroku


* sudo snap install heroku --classic


* Le fichier "Procfile" doit être à la racine l'application et contenir la ligne suivante : 'web: vendor/bin/heroku-php-apache2 public/'


* Sinon, faire : echo "web: vendor/bin/heroku-php-apache2 public/"> Procfile pour le créer

 
* heroku login


* heroku create


* git remote -v


(pour vérifier)


* heroku git:remote -a nomDuDomaine


* heroku config:set APP_KEY=NomDeLAPPKEY


* heroku addons:create cleardb:ignite


* heroku config | grep CLEARDB_DATABASE_URL


* Dans le fichier config/database.php, mettre ce qui suit :
//At the top of your file :
$url = parse_url(getenv("CLEARDB_DATABASE_URL"));
$host = $url["host"] ?? null;
$username = $url["user"] ?? null;
$password = $url["pass"] ?? null;
$database = substr($url["path"], 1);
// Just after the return [ :
'default' => env('DB_CONNECTION', 'your_heroku_mysql_connection'),
// In the 'connections' array :
'your_heroku_mysql_connection' => array(
            'driver' => 'mysql',
            'host' => $host,
            'database' => $database,
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ),


* git add . 


* git commit -am 'titre du commit'


* git push heroku master


* heroku addons:create


* heroku-postgresql:nomdelabase


*  heroku config


* Remplir les configs grâce au heroku/postgres/Settings/View credentials :


*  heroku config:set DB_CONNECTION=pgsql
* heroku config:set DB_HOST=
* heroku config:set DB_PORT=
* heroku config:set DB_DATABASE=
* heroku config:set DB_USERNAME=
* heroku config:set DB_PASSWORD=
* heroku config:set DATABASE_URL=


* heroku run php artisan migrate 


* heroku run php artisan db:seed



